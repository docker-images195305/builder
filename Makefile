CONTAINER_CLI ?= podman

build:
	$(CONTAINER_CLI) image build --tag builder:latest --tag builder:$(VERSION) .

