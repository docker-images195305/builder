FROM alpine:3.20

RUN apk add --update --no-cache \
    bash \
    curl \
    git \
    jq \
    make \
    rsync \
    openssh \
    wget
